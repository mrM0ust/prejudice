#ifndef IE
#define IE

#include "knowledgeBase.h"
#include "terminalUI.h"
#include "dataBase.h"
#include "fuzzy.h"

#include <set>
#include <iostream>
#include <map>
#include <iomanip>
#include <vector>

class InferenceEngine {

  private:
    KnowledgeBase kb;
    TerminalUI ui;
    DataBase db;
    std::set<Conclusion> possibleConclusions;
    void doFuzzy();

  public:
    void startInference();
    bool questionExists() const;
    void ask(Predicate, Conclusion);
    bool worthToAsk(const Conclusion&) const;
    void tellHow() const;
    void showProbability();
    double getNumerator(const Conclusion&) const;
    double getDenominator(const Conclusion&) const;
    bool shouldCnt(const Conclusion&, const Conclusion&) const;
    double fuzzyRules( const Conclusion&) const;
    double sum(std::vector<double>&, double);
    double sum2(std::vector<double>&, double);
};



#endif
