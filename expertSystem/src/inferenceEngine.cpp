

#include "inferenceEngine.h"

//    KnowledgeBase kb;
//    TerminalUI ui;
//    DataBase db;
//    std::set<Conclusion> possibleConclusions;
  
void InferenceEngine::startInference() {
  
  possibleConclusions = kb.getAllConclusions();

  for ( auto conclusion : kb.getAllConclusions() ) {
    for ( auto predicate : kb.getPredicates(conclusion) ) {
        if ( !db.contains(predicate) )
          ask(predicate, conclusion);
    }      
  }

  doFuzzy();


}

double InferenceEngine::fuzzyRules(const Conclusion& con) const {
  double minimum = 1, tmp;
  for( auto& i : kb.getPredicates(con) ){
    //std::cout << con.getName() << " " << i.getName() << ": je" << i.getMf() << " protoFuzzy: " << db.getAnswer(i).getF(i.getMf()) << std::endl;
    tmp = db.getAnswer(i).getF(i.getMf());
    if( i.isNegative() ) tmp = 1.0-tmp;
    minimum = minimum > tmp ? tmp : minimum; 
  }
  return minimum;
}

void InferenceEngine::doFuzzy() {

  Fuzzy fuz;

  for( auto& i : db.answers ){
    i.second.cntFuzzy(fuz.fuzzy.at(i.first));
  }

  //for( auto i : kb.getAllPredicates() ) {
  //  std::cout << i.getName() << ": " << db.getAnswer(i).getAnswer() << " ... " << db.getAnswer(i).getNever() << " " << db.getAnswer(i).getDontcare() << " " << db.getAnswer(i).getImportant() << std::endl;  
  //}
  
  std::map<Conclusion, double> tmp;

  bool chck = false;
  for( auto& i : kb.getAllConclusions() ){
    double t = fuzzyRules(i);
    tmp.insert({i, t});
    if(t != 0) chck = true;
  }

  if(!chck) {
    std::cout << "Zadny vysledek." << std::endl;
    return;
  }

  //for(auto& i : tmp) {
  //  std::cout << i.first.getName() << " " << i.second << std::endl;
  //}

  //for(auto& i : fuz.fuzzyCon ) {
  //  std::cout << i.first.getName() << ": " << std::endl;
  //  for(auto &j : fuz.fuzzyCon.at(i.first) ){
  //    std::cout << j << " ";
  //}}
 
  double sum1 = 0, suma2 = 0;
  for(auto& i : tmp) {
    if( i.second == 0 ) continue;
    sum1 += sum(fuz.fuzzyCon.at(i.first), i.second);
    suma2 += sum2(fuz.fuzzyCon.at(i.first), i.second);
  }


  double result = sum1/suma2;

  std::cout << "Vysledek: "  << result << ", tedy: ";
  
  for(auto& i : fuz.fuzzyCon) {
    if(fuz.fuzzyCon.at(i.first)[0] <= result && fuz.fuzzyCon.at(i.first)[3] >= result)
      std::cout << i.first.getName() << " ";
  }

  std::cout << std::endl;

  std::string in; 
  std::cin >> in; 
  if (in.compare("how") == 0) {
    std::cout << "Protoze ";
    for(auto& i : fuz.fuzzyCon) {
      if(fuz.fuzzyCon.at(i.first)[0] <= result && fuz.fuzzyCon.at(i.first)[3] >= result) {
        std::cout << std::endl;
        for( auto & j : kb.getPredicates(i.first) ) {
          if( j.isNegative() ) std::cout << "nema ";
          else std::cout << "ma ";
          if(j.getMf() == 0) {std::cout << "otresny ";}
          else if (j.getMf() == 1) {std::cout << "prumerny ";}
          else {std::cout << "vyborny ";}
          std::cout << j.getName() << ", ";
        }
      }
    }
    std::cout << std::endl;
  }
  else if (in.compare("stop") == 0) return;

  

}

double InferenceEngine::sum2(std::vector<double>& abcd, double limit){
 
  int a = (int)abcd[0], b = (int)abcd[1], c = (int)abcd[2], d = (int)abcd[3];
  double sum = 0;

  if(a==b) {
    for( int i = a+1; i < d; ++i ){
      if( i <= c ) sum += limit;
      else {
        double tmp = (i-d)/(c-d);
        sum += tmp > limit ? limit : tmp; 
      } 
    }  
  return sum;
  }

  else if (c==d) {
    for( int i = a+1; i < d; ++i ){
      if(i >= b) sum += limit;
      else {
        double tmp = (i-a)/(b-a);
        sum += tmp > limit ? limit : tmp;
      }
    }
  return sum;
  }

  for( int i = a+1; i < d; ++i ){
    double tmp = std::min((i-a)/(b-a), (d-i)/(d-c));
    sum += tmp > limit ? limit : tmp;
  }
  return sum;
}

double InferenceEngine::sum(std::vector<double>& abcd, double limit){
 
  int a = (int)abcd[0], b = (int)abcd[1], c = (int)abcd[2], d = (int)abcd[3];
  double sum = 0;

  if(a==b) {
    for( int i = a+1; i < d; ++i ){
      if( i <= c ) sum += i*limit;
      else {
        double tmp = (i-d)/(c-d);
        sum += tmp > limit ? i*limit : i*tmp; 
      } 
    }  
  return sum;
  }

  else if (c==d) {
    for( int i = a+1; i < d; ++i ){
      if(i >= b) sum += i*limit;
      else {
        double tmp = (i-a)/(b-a);
        sum += tmp > limit ? i*limit : i*tmp;
      }
    }
  return sum;
  }

  for( int i = a+1; i < d; ++i ){
    double tmp = std::min((i-a)/(b-a), (d-i)/(d-c));
    sum += tmp > limit ? i*limit : i*tmp;
  }
  return sum;
}

void InferenceEngine::showProbability() {
  
  std::multimap<double,Conclusion> probs;
  double sum = 0;

  for( auto& i : possibleConclusions ) {
    double p = (getNumerator(i)/getDenominator(i));
    probs.insert({p, i});
    sum+=p;
    //std::cout << p << " " << i.getName() << std::endl;
    //std::cout << sum << std::endl;
  }
  
  int cnt = 0;
  std::string in;
  for (auto i = probs.rbegin(); i != probs.rend(); ++i) {
    std::cout << std::setprecision (3) << (i->first/sum)*100 << "% " << i->second.getName() << std::endl;
    ++cnt;
    if( cnt == 3 ) {
      std::cout << "That's only top 3. Show all?[y]" << std::endl;
      std::cin >> in;
      if (in.compare("y") != 0) break;
    }
  }

}

double InferenceEngine::getNumerator(const Conclusion& conclusion) const {
  double p = conclusion.getUncertainty();

  for( auto& i : kb.getPredicates(conclusion) ){
    if( i.isNegative() ) p*=i.getUncertainty()*(1-db.getAnswer(i).getAnswer());
    else p*=i.getUncertainty()*db.getAnswer(i).getAnswer();
    //std::cout << i.getName() << " " << db.getAnswer(i).getAnswer() << std::endl;
    //std::cout << p << " *= " << i.getUncertainty() << " * " << db.getAnswer(i).getAnswer() << std::endl;
  }
  //std::cout << p;
  return p;
}

double InferenceEngine::getDenominator(const Conclusion& conclusion) const {

  std::set<Predicate> preds = kb.getPredicates(conclusion);
  
  double p = 0, tmp;

  for( auto& i : kb.getAllConclusions()){
    if(!shouldCnt(conclusion, i))
      continue;
    tmp = i.getUncertainty();
    for( auto& j : kb.getPredicates(i) ){
      if( preds.count(j) == 0) continue;
      if( j.isNegative() ) tmp *= j.getUncertainty()*(1-db.getAnswer(j).getAnswer());
      else tmp *= j.getUncertainty()*db.getAnswer(j).getAnswer();
    }
    //std::cout << "tmp.." << tmp << std::endl;
    p+=tmp;
  }
  //std::cout << "/" << p << std::endl;

return p;
}

bool InferenceEngine::shouldCnt(const Conclusion& a, const Conclusion& b) const {

  std::set<Predicate> left = kb.getPredicates(a);
  std::set<Predicate> right = kb.getPredicates(b);

  for(auto &i : left) {
    if( right.count(i) == 0 ) return false;
  }
  //std::cout << a.getName() << " " << b.getName() << std::endl;
return true;
}


void InferenceEngine::tellHow() const {

  for( auto i : possibleConclusions ) {
    std::cout << i.getName() << ", because you" << std::endl;
    for( auto j : kb.getPredicates(i) ){
      if( j.isNegative() ) std::cout << "\tdon't like " << j.getName() << std::endl;
      else std::cout << "\tlike " << j.getName() << std::endl;
    }
  }

}

bool InferenceEngine::worthToAsk(const Conclusion& conclusion) const {
  
  for( auto i : kb.getPredicates(conclusion) ){
    if( db.contains(i) ) {
      if( !kb.query( db.getAnswer(i), conclusion) )
        return false;
    }
  }
  return true;

}


void InferenceEngine::ask(Predicate question, Conclusion possible) {
  try {
    db.addAnswer(ui.askUser(Predicate(question)));
  } 
  catch (std::string e) {
    if ( !e.compare("why") )
      std::cout << "Tohle je fuzzy, nejdrive odpovez na otazky " << std::endl;
    else if ( !e.compare("how") )
      std::cout << "Pocekj na vyhodnoceni!" << std::endl;
    else
      std::cout << "coze?!" << std::endl;
    ask(question, possible);
  }
}


bool InferenceEngine::questionExists() const {
  
  for(auto conclusion : possibleConclusions) {
    for(auto predicate : kb.getPredicates(conclusion)){
      if(!db.contains(predicate))
        return true;
    }
  }
  return false;
}


