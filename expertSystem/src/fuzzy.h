#ifndef FUZZY
#define FUZZY

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include "elements.h"

class Fuzzy {

  private:
    void parseLine(std::string&);
    void parseCon(std::string&);

  public:
    Fuzzy();
    std::map<Predicate, std::vector<double>> fuzzy;
    std::map<Conclusion, std::vector<double>> fuzzyCon;


};



#endif

