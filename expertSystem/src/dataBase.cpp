#include "dataBase.h"

Answer DataBase::getAnswer(const Predicate& predicate) const {
  return answers.at(predicate);
}

bool DataBase::contains(const Predicate& predicate) const {
  return answers.count(predicate);
}

void DataBase::addAnswer(Answer answer) {
  answers.insert({answer.getPredicate(), answer});
}
