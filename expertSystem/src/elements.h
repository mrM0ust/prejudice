#ifndef ELEMENTS
#define ELEMENTS

#include <string>
#include <iostream>
#include <algorithm>
#include <vector>

class Conclusion {
  private:
    std::string name;
    double uncertainty;
  
  public:
    Conclusion(std::string);
    std::string getName() const;
    double getUncertainty() const;
    void setUncertainty(double);
    bool operator< (const Conclusion&) const;
};

class Predicate {
  private:
    std::string name;
    bool negation = false;
    double uncertainty;
    int mf;

  public:
    Predicate(std::string);
    std::string getName() const;
    double getUncertainty() const;
    bool isNegative() const;
    bool operator< (const Predicate&) const;
    int getMf() const;
};

class Answer {
  private:
    Predicate predicate;
    double answer;
    double never, dontcare, important;
    double getFVal(double, double, double, double);

  public:
    Answer(Predicate, double);
    Predicate getPredicate();
    void cntFuzzy(std::vector<double>&);
    double getNever();
    double getDontcare();
    double getImportant();
    double getAnswer();
    double getF(int);

};


#endif
