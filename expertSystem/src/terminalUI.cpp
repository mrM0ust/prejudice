

#include "terminalUI.h"

Answer TerminalUI::askUser(const Predicate& predicate) {

  std::string userInput;

  std::cout << "Jak hodnotis " << predicate.getName() << "? (0-10)" << std::endl;

  std::cin >> userInput;

  //if (!userInput.compare("yes") || !userInput.compare("y")) {
    //std::cout << "yes" << std::endl;
  //  return Answer(predicate, 1.0);
  //}
  //else if (!userInput.compare("no") || !userInput.compare("n")) {
    //std::cout << "no" << std::endl;
  //  return Answer(predicate, 0.0);
  //}
  //else if (!userInput.compare("idk")) {
    //std::cout << "idk" << std::endl;
  //  return Answer(predicate, 0.5);
  //}
  if (!userInput.compare("why")) {
    //std::cout << "why" << std::endl;
    throw std::string("why");
  }
  else if (!userInput.compare("how")) {
    //std::cout << "how" << std::endl;
    throw std::string("how");
  }
  else if (!userInput.compare("stop")) {
    //std::cout << "stop" << std::endl;
    throw "stop";
  }
  double value;
  try {
    value = std::stod(userInput);
  } catch (...) {
    std::cout << "Spatna odpoved!" << std::endl;
    return askUser(predicate);
  }
  if (value >= 0.0 && value <= 10.0) {
    return Answer(predicate, value);
  }
  else {
    std::cout << "Spatna odpoved!" << std::endl;
    return askUser(predicate);
  }
}
