
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <vector>

using namespace std;

std::vector<double> values;

void parseLine(std::string &line) {

  vector<string> v;
  
    istringstream buf(line);
    for(string word; buf >> word; )
        v.push_back(word);
    
    for( uint32_t i = 1; i < v.size(); ++i )
        values.push_back(stod(v[i]));
   
    

}

int main(){


  std::string line;
  std::ifstream file ("./RulesTrapezodials");

  while(std::getline(file, line)) {
    //std::cout << line << std::endl;
    parseLine(line);
  }

  file.close();


  for(auto i : values ) std::cout << i << std::endl;
  
  return 0;
}
