#include "fuzzy.h"

Fuzzy::Fuzzy() {


  std::string line;
  std::ifstream file ("./src/PredTrapezodials");
  std::ifstream file2 ("./src/ConTrapezodials");

  while(std::getline(file, line)) {
    //std::cout << line << std::endl;
    parseLine(line);
  }

  while(std::getline(file2, line))
    parseCon(line);

  file.close();
  file2.close();

  /*
  for( auto i : fuzzy ) {
    std::cout << i.first.getName() << std::endl;
    for ( auto j : fuzzy.at(i.first) )
      std::cout << j << std::endl;
  }
  */


}

void Fuzzy::parseCon(std::string& line) {

  std::vector<double> values;
  std::vector<std::string> v;
  std::istringstream buf(line);
  for(std::string word; buf >> word; ) {
    v.push_back(word);
  }

  for( uint32_t i = 1; i < v.size(); ++i ){
    values.push_back(stod(v[i]));
  }

  fuzzyCon.insert ({Conclusion(v[0]), values});

}

void Fuzzy::parseLine(std::string& line) {

  std::vector<double> values;
  std::vector<std::string> v;
  std::istringstream buf(line);
  for(std::string word; buf >> word; ) {
    v.push_back(word);
  }

  for( uint32_t i = 1; i < v.size(); ++i ){
      values.push_back(stod(v[i]));
  }

  fuzzy.insert ({Predicate(v[0]), values});

}

