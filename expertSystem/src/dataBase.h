#ifndef DATABASE
#define DATABASE

#include "elements.h"
#include <map>

class DataBase {

    
  public:
    Answer getAnswer(const Predicate&) const;
    bool contains(const Predicate&) const;
    void addAnswer(Answer);
    std::map<Predicate, Answer> answers;

};

#endif
