#include "elements.h"

 
Conclusion::Conclusion(std::string name) : name(name) {}

std::string Conclusion::getName() const {
  return this->name;
}

double Conclusion::getUncertainty() const {
  return this->uncertainty;
}

void Conclusion::setUncertainty(double unc) {
  this->uncertainty = unc;
}

bool Conclusion::operator< (const Conclusion& obj) const {
  return(this->name < obj.getName());
}

//---------------------------------------------------------------

Predicate::Predicate(std::string name) {
  if (name[0] == '!') {
    name.erase(0, 1);
    this->negation = true;
  }
  //std::cout << name << std::endl;
  this->mf = name[name.length()-1];
  this->mf = this->mf-48;
  this->name = name.substr(0, name.find_first_of("012"));;
  //std::cout << this->mf << std::endl;
}

std::string Predicate::getName() const {
  return this->name;
}

double Predicate::getUncertainty() const {
  return this->uncertainty;
}

bool Predicate::isNegative() const {
  return this->negation;
}

bool Predicate::operator< (const Predicate& obj) const {
  return(this->name < obj.getName());
}

int Predicate::getMf() const {
  return this->mf;
}


//-------------------------------------------------------------

Answer::Answer(Predicate predicate, double answer) : predicate(predicate), answer(answer) {}

Predicate Answer::getPredicate() {
return this->predicate;
}

double Answer::getFVal(double a, double b, double c, double d) {
  if (a == b) {
    if (this->answer <= c) return 1.0;
    if (this->answer >= d) return 0.0;
    //std::cout << (this->answer-d)/(c-d) << std::endl;
    return (this->answer-d)/(c-d);
  }
  if (c == d) {
    if (this->answer <= a) return 0.0;
    if (this->answer >= b) return 1.0;
    return (this->answer-a)/(b-a);
  }

  double minimum = std::min((this->answer-a)/(b-a), (d-this->answer)/(d-c));
  if ( minimum > 1 ) minimum = 1; 
  return std::max(minimum, 0.0);
}

void Answer::cntFuzzy(std::vector<double>& values) {
  never = getFVal(values[0], values[1], values[2], values[3]);
  dontcare = getFVal(values[4], values[5], values[6], values[7]);
  important = getFVal(values[8], values[9], values[10], values[11]);
}

double Answer::getF(int x){
  if ( x == 0)
    return getNever();
  else if (x == 1)
    return getDontcare();
  return getImportant();
}

double Answer::getNever() {
  return this->never;
}
double Answer::getDontcare() {
  return this->dontcare;
}
double Answer::getImportant() {
  return this->important;
}

double Answer::getAnswer() {
return this->answer;    
}



