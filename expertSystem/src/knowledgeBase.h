#ifndef KNOWLEDGEBASE
#define KNOWLEDGEBASE

#include "elements.h"
#include <set>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>

class KnowledgeBase {

  private:
    std::map<Conclusion, std::set<Predicate>> rules;
    void parseLine(std::string&);

  public:
    KnowledgeBase();
    std::set<Conclusion> getAllConclusions() const;
    std::set<Predicate> getAllPredicates() const;
    std::set<Predicate> getPredicates(Conclusion conclusion) const;
    bool query(Answer, Conclusion) const;
};

#endif
