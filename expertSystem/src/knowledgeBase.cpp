
#include "knowledgeBase.h"

KnowledgeBase::KnowledgeBase() {
  std::string line;
  std::ifstream file ("./src/RulesKnowledgeBase");

  while(std::getline(file, line)) {
    //std::cout << line << std::endl;
    parseLine(line);
  }
  
  file.close();
}

void KnowledgeBase::parseLine(std::string& line ) {

  std::set<Predicate> predicates;

  std::regex regex("^IF (\\S+)");
  std::smatch con;
  std::regex_search(line, con, regex);
   
  Conclusion conclusion(con.str(1));

  line.erase(0, 9 + con.str(1).length());

  std::regex regex2("^(\\S+).*");
  std::smatch predicate;

  while(line.length() > 0) {
    std::regex_search(line, predicate, regex2);
    try {
      predicates.insert(predicate.str(1));
    }
    catch (std::string e) {
      std::cout << e << std::endl;
      std::exit(1);
    }
    line.erase(0, 4  + predicate.str(1).length());
  }

  rules.insert({conclusion, predicates});

}

std::set<Conclusion> KnowledgeBase::getAllConclusions() const {
  std::set<Conclusion> conclusions;

  for( auto i : rules ){
    conclusions.insert(i.first);
  }

return conclusions;
}

std::set<Predicate> KnowledgeBase::getAllPredicates() const {
  std::set<Predicate> predicates;
  
  for( auto i : rules ){
    predicates.insert(i.second.begin(), i.second.end());
  }

return predicates;
}

std::set<Predicate> KnowledgeBase::getPredicates(Conclusion conclusion) const {

return rules.at(conclusion);
}

bool KnowledgeBase::query(Answer answer, Conclusion conclusion) const { //prijede odpoved na dany predikat a conclusion pro ktery provedu kontrolu

  if( answer.getAnswer() == 0.0 && !rules.at(conclusion).find(answer.getPredicate())->isNegative() ) return false;
  if( answer.getAnswer() != 0.0 && rules.at(conclusion).count(answer.getPredicate()) == 0 ) return false;
  if( answer.getAnswer() != 0.0 && rules.at(conclusion).find(answer.getPredicate())->isNegative() ) return false;


  return true;
//return rules.at(conclusion).contains(predicate);
}
