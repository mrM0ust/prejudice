#ifndef UI
#define UI

#include "elements.h"
#include <iostream>
#include <string>

class TerminalUI {

  public:
    Answer askUser(const Predicate&);
};


#endif
